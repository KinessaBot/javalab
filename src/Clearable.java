/**
 * Created by Seni on 10/7/2018.
 */
public interface Clearable
{
    void clear() throws UnclearableException;
}
