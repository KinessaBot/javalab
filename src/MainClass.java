
/**
 * Created by  @author Seni on 10/1/2018.
 * @version 1.0
 */
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

//Action class
public class MainClass {

    private Record [] records = new Record[100];
    private int i = 0;
    private String Path;
    private int Type;

    public MainClass(Record[] records, int i, String path, int type) {
        this.records = records;
        this.i = i;
        Path = path;
        Type = type;
    }

    public MainClass() {
        this.records = new Record[100];
        this.i = 0;
        Path = "";
        Type = 0;
    }

    /**
     * Main menu
     */
    public void main(){
        while(true)
        {
            System.out.println("1. Read from json");
            System.out.println("2. Read from xml");
            System.out.println("3. Read from console");
            System.out.println("4. Generate random elements");
            System.out.println("5. Show tables");
            System.out.println("6.Exit");
            System.out.println("___________________");
            Scanner in = new Scanner(System.in);
            try {
                switch (in.nextInt()) {
                    case 1:
                        FromJSON();
                        break;
                    case 2:
                        FromXML();
                        break;
                    case 3:
                        FromConsole();
                        break;
                    case 4:
                        GenerateRandom();
                        break;
                    case 5:
                        Show();
                        break;
                    case 6:
                        return;
                    default:
                        System.out.println("incorrect value");
                        break;
                }
            }
            catch (Exception e)
            { e.printStackTrace();return;}
        }

    }

    /**
     *  Show's Table
     */
    private void Show()
    {
        Sort();
        for (int j=0;j<i;j++)
        {
            System.out.println(records[j].ToString());
        }

    }
    /**
     *  Generates Random
     */
    public void GenerateRandom()
    {
        RequestType();
        Scanner in = new Scanner(System.in);
        System.out.println("Print amount");
        int k = in.nextInt();
        switch (Type) {
            case 1: {
                for(int j=0; j<k; j++) {
                    records[i] = new Child();
                    records[i].CreateRandom();
                    i++;
                }
                break;
            }
            case 2: {
                for(int j=0; j<k; j++) {
                    records[i] = new Guide();
                    records[i].CreateRandom();
                    i++;
                }
                break;
            }
            case 3: {
                for(int j=0; j<k; j++) {
                    records[i] = new Arrival();
                    records[i].CreateRandom();
                    i++;
                }
                break;
            }
            case 4:
                return;
            default:
                System.out.println("Type does not exist");
        }
    }
    /**
     *  Sort. Hashcode as key
     */
    private void Sort()
    {
        for(int j1=0; j1<i; j1++)
            for(int j=0; j<i-1; j++)
                if(records[j].hashCode()>records[j+1].hashCode())
                {
                    Record x = records[j];
                    records[j]=records[j+1];
                    records[j+1]=x;
                }
    }
    /**
     *  Reading drom console
     */
    private void FromConsole()
    {
        RequestType();
        switch (Type) {
            case 1: {
                records[i] = new Child();
                records[i].ReadConsole();
                i++;
                break;
            }
            case 2: {
                records[i] = new Guide();
                records[i].ReadConsole();
                i++;
                break;
            }
            case 3: {
                records[i] = new Arrival();
                records[i].ReadConsole();
                i++;
                break;
            }
            case 4:
                return;
            default:
                System.out.println("Type does not exist");
        }

    }
    /**
     *  Reading from text
     */
    private void FromJSON() {
        RequestPath();
        RequestType();
        JSONParser parser = new JSONParser();
        try {
            JSONArray array = (JSONArray) parser.parse(new FileReader(Path));
            switch (Type) {
                case 1: {
                    for (Object o : array) {
                        records[i] = new Child();
                        records[i].ReadJSON((JSONObject) o);
                        i++;
                    }
                    break;
                }
                case 2: {
                    for (Object o : array) {
                        records[i] = new Guide();
                        records[i].ReadJSON((JSONObject) o);
                        i++;
                    }
                    break;
                }
                case 3: {
                    for (Object o : array) {
                        records[i] = new Arrival();
                        records[i].ReadJSON((JSONObject) o);
                        i++;
                    }
                    break;
                }
                case 4:
                    return;
                default:
                    System.out.println("Type does not exist");
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }
    /**
     *  Requesting Path
     */
    private String RequestPath()
    {
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Print Path");
            Path = in.nextLine();
            return Path;
        }
        catch(Exception e)
        {
            e.getStackTrace();
        }
        return null;
    }
    /**
     *  Requesting type
     */
    private int RequestType()
    {
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("___________________");
            System.out.println("1. Child");
            System.out.println("2. Guide");
            System.out.println("3. Arrival");
            System.out.println("4. /return");
            System.out.println("___________________");
            Type = in.nextInt();
            return Type;
        }
        catch(Exception e)
        {
            e.getStackTrace();
        }
        return -1;
    }
    /**
     *  Reading from xml
     */
    private void FromXML()
    {
        RequestPath();
        RequestType();
        try {
            FileReader in = new FileReader(Path);
            Scanner scan = new Scanner(in);

            switch (Type) {
                case 1 : {
                    while(scan.hasNextLine())
                    {
                        records[i] = new Child();
                        records[i].ReadXML(scan);
                        i++;
                    }
                    break;
                }
                case 2 : {
                    while(scan.hasNextLine())
                    {
                        records[i] = new Guide();
                        records[i].ReadXML(scan);
                        i++;
                    }
                    break;
                }
                case 3 : {
                    while(scan.hasNextLine())
                    {
                        records[i] = new Arrival();
                        records[i].ReadXML(scan);
                        i++;
                    }
                    break;
                }
                case 4 : return;
                default: System.out.println("Type does not exist");
            }
            in.close();
            scan.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
