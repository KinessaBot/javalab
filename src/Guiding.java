import com.sun.prism.impl.Disposer;
import org.json.simple.JSONObject;

import java.util.Scanner;

/**
 * Created by Seni on 10/9/2018.
 */
public class Guiding extends Record {

    Integer ChieldsID;
    Integer GuidsID;
    Integer ArrivlID;

    public Integer getChieldsID() {
        return ChieldsID;
    }

    public void setChieldsID(Integer chieldsID) {
        ChieldsID = chieldsID;
    }

    public Integer getGuidsID() {
        return GuidsID;
    }

    public void setGuidsID(Integer guidsID) {
        GuidsID = guidsID;
    }

    public Integer getArrivlID() {
        return ArrivlID;
    }

    public void setArrivlID(Integer arrivlID) {
        ArrivlID = arrivlID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Guiding)) return false;

        Guiding guiding = (Guiding) o;

        if (ChieldsID != null ? !ChieldsID.equals(guiding.ChieldsID) : guiding.ChieldsID != null) return false;
        if (GuidsID != null ? !GuidsID.equals(guiding.GuidsID) : guiding.GuidsID != null) return false;
        return ArrivlID != null ? ArrivlID.equals(guiding.ArrivlID) : guiding.ArrivlID == null;
    }

    @Override
    public int hashCode() {
        int result = ChieldsID != null ? ChieldsID.hashCode() : 0;
        result = 31 * result + (GuidsID != null ? GuidsID.hashCode() : 0);
        result = 31 * result + (ArrivlID != null ? ArrivlID.hashCode() : 0);
        return result;
    }
    public String ToString() {
        return "ArrivlID " + ArrivlID + " ChieldsID " + ChieldsID+ " GuidsID " + GuidsID;
    }

    /**
     *
     * @return
     */
    public Record ReadConsole()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Print Childes ID ");
        ChieldsID = in.nextInt();
        System.out.println("Print Guides ID ");
        GuidsID = in.nextInt();
        System.out.println("Print Arival ID ");
        ArrivlID = in.nextInt();
        return this;
    }

    /**
     *
     * @param obj
     * @return
     */
    public Record ReadJSON(JSONObject obj)
    {
        ChieldsID = ((Long)obj.get("ChieldsID")).intValue();
        GuidsID = ((Long)obj.get("GuidsID")).intValue();
        ArrivlID = ((Long)obj.get("ArrivlID")).intValue();
        return this;
    }

    /**
     *
     * @param scan
     * @return
     */
    public Record ReadXML(Scanner scan)
    {
        scan.nextLine();
        ChieldsID = Integer.valueOf(scan.nextLine().split("<.*?>")[1]);
        GuidsID = Integer.valueOf(scan.nextLine().split("<.*?>")[1]);
        ArrivlID = Integer.valueOf(scan.nextLine().split("<.*?>")[1]);
        scan.nextLine();
        return this;
    }

    /**
     *
     * @return
     */
    public Record CreateRandom()
    {
        ChieldsID = (int)(Math.random()*100);
        GuidsID = (int)(Math.random()*100);
        ArrivlID = (int)(Math.random()*100);
        return this;
    }
}
