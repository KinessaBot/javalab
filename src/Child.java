/**
 * Created by  @author Seni on 10/1/2018.
 * @version 1.0
 */
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.json.simple.*;

import java.util.Scanner;


public class Child extends Record implements Clearable
{
    private Integer ID;
    private String FIO;

    Child() {
        ID = -1;
        FIO = "";
    }

    public Child(Integer ID, String FIO) {
        this.ID = ID;
        this.FIO = FIO;
    }

    public Integer GetID() {
        return ID;
    }

    public String GetFIO() {
        return FIO;
    }

    public Child SetID(Integer ID) {
        this.ID = ID;
        return this;
    }

    public Child SetFIO(String FIO) {
        this.FIO = FIO;
        return this;
    }

    /**
     *
     * @return
     */
    public String ToString() {
        return "FIO " + FIO + " ID " + ID;
    }

    /**
     *
     * @return
     */
    public Record ReadConsole()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Print Childes ID ");
        ID = in.nextInt();
        System.out.println("Print Childes FIO ");
        FIO = in.nextLine();
        return this;
    }

    /**
     *
     * @param obj
     * @return
     */
    public Record ReadJSON(JSONObject obj)
    {
        ID = ((Long)obj.get("ID")).intValue();
        FIO = (String)obj.get("FIO");
        return this;
    }

    /**
     *
     * @param scan
     * @return
     */
    public Record ReadXML(Scanner scan)
    {
        scan.nextLine();
        ID = Integer.valueOf(scan.nextLine().split("<.*?>")[1]);
        FIO = scan.nextLine().split("<.*?>")[1];
        scan.nextLine();
        return this;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Child)) return false;

        Child child = (Child) o;

        if (!ID.equals(child.ID)) return false;
        return FIO.equals(child.FIO);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int result = ID.hashCode();
        result = 31 * result + FIO.hashCode();
        return result;
    }

    /**
     *
     * @return
     */
    public Record CreateRandom()
    {
        ID = (int)(Math.random()*100);
        int k = (int)(Math.random()*19)+1;
        StringBuilder stringBuffer = new StringBuilder();
        for(int i=0; i<k; i++)
        {
            stringBuffer.append((char)(Math.random()*46+42));
        }
        FIO = stringBuffer.toString();
        return this;
    }

    /**
     *
     * @throws UnclearableException
     */
    public void clear() throws UnclearableException {
        if (ID==-1)
        {
            throw new UnclearableException();
        }
        ID = -1;
        FIO = "";
    }

}
