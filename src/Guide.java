/**
 * Created by  @author Seni on 10/1/2018.
 * @version 1.0
 */
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.json.simple.*;
import java.util.Scanner;

public class Guide extends Record implements Clearable

{
    private Integer ID;
    private String FIO;
    private Float Salary;

    Guide() {
        ID = -1;
        FIO = "";
        Salary = 0f;
    }

    public Guide(Integer ID, String FIO, Float Salary) {
        this.ID = ID;
        this.FIO = FIO;
        this.Salary = Salary;
    }

    public Integer GetID() {
        return ID;
    }

    public String GetFIO() {
        return FIO;
    }

    public Float GetSalary() {
        return Salary;
    }

    public Guide SetID(Integer ID) {
        this.ID = ID;
        return this;
    }

    public Guide SetFIO(String FIO) {
        this.FIO = FIO;
        return this;
    }

    public Guide SetSalary(Float Salary) {
        this.Salary = Salary;
        return this;
    }

    /**
     *
     * @return
     */
    public String ToString() {
        return "FIO " + FIO + " ID " + ID + " Salary "+ Salary;
    }

    /**
     *
     * @return
     */
    public Record ReadConsole() {
        Scanner in = new Scanner(System.in);
        System.out.println("Print Guides ID ");
        ID = in.nextInt();
        System.out.println("Print Guides FIO ");
        FIO = in.next();
        System.out.println("Print Guides Salary ");
        Salary = in.nextFloat();
        return this;
    }

    /**
     *
     * @param obj
     * @return
     */
    public Record ReadJSON(JSONObject obj) {
        ID = ((Long)obj.get("ID")).intValue();
        FIO = (String) obj.get("FIO");
        Salary = ((Double)obj.get("Salary")).floatValue();
        return this;
    }

    /**
     *
     * @param scan
     * @return
     */
    public Record ReadXML(Scanner scan) {
        scan.nextLine();
        ID = Integer.valueOf(scan.nextLine().split("<.*?>")[1]);
        FIO = scan.nextLine().split("<.*?>")[1];
        Salary = Float.valueOf(scan.nextLine().split("<.*?>")[1]);
        scan.nextLine();
        return this;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Guide)) return false;

        Guide guide = (Guide) o;

        if (!ID.equals(guide.ID)) return false;
        if (!FIO.equals(guide.FIO)) return false;
        return Salary.equals(guide.Salary);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int result = ID.hashCode();
        result = 31 * result + FIO.hashCode();
        result = 31 * result + Salary.hashCode();
        return result;
    }

    /**
     *
     * @return
     */
    public Record CreateRandom()
    {
        ID = (int)(Math.random()*100);
        int k = (int)(Math.random()*19)+1;
        StringBuilder stringBuffer = new StringBuilder();
        for(int i=0; i<k; i++)
        {
            stringBuffer.append((char)(Math.random()*46+42));
        }
        FIO = stringBuffer.toString();
        Salary = (float)(Math.random()*100);
        return this;
    }

    /**
     *
     * @throws UnclearableException
     */
    public void clear() throws UnclearableException {
        if (ID==-1)
        {
            throw new UnclearableException();
        }
        ID = -1;
        FIO = "";
        Salary = 0f;
    }
}
