/**
 * Created by  @author Seni on 10/1/2018.
 * @version 1.0
 */
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.json.simple.*;

import java.util.Scanner;

/**
 *
 */
public class Arrival extends Record implements Clearable

{
    private Integer ID;
    private Float Start;
    private Float End;

    Arrival() {
        ID = -1;
        Start = 0f;
        End = -1f;
    }

    /**
     *
     * @param ID
     * @param Start
     * @param End
     */
    public Arrival(Integer ID, Float Start, Float End) {
        this.ID = ID;
        this.Start = Start;
        this.End = End;
    }

    public Integer GetID() {
        return ID;
    }

    public Float GetStart() {
        return Start;
    }

    public Float GetEnd() {
        return End;
    }

    public Arrival SetID(Integer ID) {
        this.ID = ID;
        return this;
    }

    public Arrival SetStart(Float Start) {
        this.Start = Start;
        return this;
    }

    public Arrival SetEnd(Float End) {
        this.End = End;
        return this;
    }

    /**
     *
     * @return
     */
    public String ToString() {
        return "Start " + Start + " END " + End + " ID " + ID;
    }

    /**
     *
     * @return
     */
    public Record ReadConsole() {
        Scanner in = new Scanner(System.in);
        System.out.println("Print Arrivals ID ");
        ID = in.nextInt();
        System.out.println("Print Arrivals Start ");
        Start = in.nextFloat();
        System.out.println("Print Arrivals End ");
        End = in.nextFloat();
        return this;
    }

    /**
     *
     * @param obj
     * @return
     */
    public Record ReadJSON(JSONObject obj) {
        ID = ((Long)obj.get("ID")).intValue();
        Start =  ((Double)obj.get("Start")).floatValue();
        End =  ((Double)obj.get("End")).floatValue();
        return this;
    }

    /**
     *
     * @param scan
     * @return
     */
    public Record ReadXML(Scanner scan) {
        scan.nextLine();
        ID = Integer.valueOf(scan.nextLine().split("<.*?>")[1]);
        Start = Float.valueOf(scan.nextLine().split("<.*?>")[1]);
        End = Float.valueOf(scan.nextLine().split("<.*?>")[1]);
        scan.nextLine();
        return this;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Arrival)) return false;

        Arrival arrival = (Arrival) o;

        if (!ID.equals(arrival.ID)) return false;
        if (!Start.equals(arrival.Start)) return false;
        return End.equals(arrival.End);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int result = ID.hashCode();
        result = 31 * result + Start.hashCode();
        result = 31 * result + End.hashCode();
        return result;
    }

    /**
     *
     * @return
     */
    public Record CreateRandom()
    {
        ID = (int)(Math.random()*100);
        Start = (float)(Math.random()*100);
        End = (float)(Math.random()*100);
        return this;
    }

    /**
     *
     * @throws UnclearableException
     */
    public void clear() throws UnclearableException {
        if (ID==-1)
        {
            throw new UnclearableException();
        }
        ID = -1;
        Start = 0f;
        End = -1f;
    }
}
