import org.json.simple.JSONObject;
import org.w3c.dom.Node;

import java.util.Scanner;

/**
 * Created by  @author Seni on 10/1/2018.
 * @version 1.0
 */
public abstract class Record {
    /**
     *
     * @param obj
     * @return
     */
    abstract public Record ReadJSON(JSONObject obj);

    /**
     *
     * @param scan
     * @return
     */
    abstract public Record ReadXML(Scanner scan);

    /**
     *
     * @return
     */
    abstract public Record ReadConsole();

    /**
     *
     * @return
     */
    abstract public Record CreateRandom();

    /**
     *
     * @return
     */
    abstract public String ToString();
}
